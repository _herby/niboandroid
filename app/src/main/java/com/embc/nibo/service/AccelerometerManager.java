package com.embc.nibo.service;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import com.embc.nibo.model.AcceleratorEventType;
import com.embc.nibo.model.AccelerometerEvent;
import com.embc.nibo.model.IntensityType;

import de.greenrobot.event.EventBus;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Listens to sensor events and acts according to them
 */
public class AccelerometerManager implements SensorEventListener {

    private final SensorManager sensorManager;
    private final Sensor acccelerometer;
    private float[] lastSensorValues;
    private final EventBus eventBus;

    public AccelerometerManager(final SensorManager sensorManager, final EventBus eventBus) {
        this.sensorManager = sensorManager;
        this.acccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.lastSensorValues = new float[3];
        this.eventBus = eventBus;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //skip other types of sensors
        if(event.sensor.getType() != Sensor.TYPE_ACCELEROMETER) {
           return;
        }

        final float x = event.values[0];
        final float y = event.values[1];
        AcceleratorEventType acceleratorEventType;
        IntensityType intensityType;

        if(x < -1) {
            if(y > 1) {
                acceleratorEventType = AcceleratorEventType.RIGHT_BACKWARDS;
                intensityType = IntensityType.typeForTilt((Math.abs(x) + Math.abs(y)) / 2);
            } else if(y < -1) {
                acceleratorEventType = AcceleratorEventType.RIGHT_FORWARDS;
                intensityType = IntensityType.typeForTilt((Math.abs(x) + Math.abs(y)) / 2);
            } else {
                acceleratorEventType = AcceleratorEventType.RIGHT;
                intensityType = IntensityType.typeForTilt(x);
            }
        } else if (x > 0) {
            if(y > 1) {
                acceleratorEventType = AcceleratorEventType.LEFT_BACKWARDS;
                intensityType = IntensityType.typeForTilt((Math.abs(x) + Math.abs(y)) / 2);
            } else if(y < -1) {
                acceleratorEventType = AcceleratorEventType.LEFT_FORWARDS;
                intensityType = IntensityType.typeForTilt((Math.abs(x) + Math.abs(y)) / 2);
            } else {
                acceleratorEventType = AcceleratorEventType.LEFT;
                intensityType = IntensityType.typeForTilt(x);
            }
        } else {
            if(y > 1) {
                acceleratorEventType = AcceleratorEventType.BACKWARDS;
                intensityType = IntensityType.typeForTilt(y);
            } else if(y < -1) {
                acceleratorEventType = AcceleratorEventType.FORWARDS;
                intensityType = IntensityType.typeForTilt(y);
            } else {
                acceleratorEventType = AcceleratorEventType.STANDING;
                intensityType = IntensityType.NONE;
            }
        }

        System.arraycopy(event.values, 0, lastSensorValues, 0, event.values.length);

        eventBus.post(new AccelerometerEvent(intensityType, acceleratorEventType));
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        System.out.println(accuracy);
    }

    public void registerListerner() {
        sensorManager.registerListener(this, acccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unRegisterListerner() {
        sensorManager.unregisterListener(this);
    }
}
