package com.embc.nibo.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import com.embc.nibo.model.ConnectEvent;
import com.embc.nibo.model.ConnectEventType;
import com.embc.nibo.model.IncidentEvent;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.Buffer;
import java.util.Queue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;

import de.greenrobot.event.EventBus;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Manages bluetooth related activities
 */
public class BluetoothManager {

    private final BluetoothAdapter bluetoothAdapter;
    private final UUID serviceUUID;
    private final EventBus eventBus;
    //unused due to lack of time
    private final RedisManager redisManager;

    private Connector connector;
    private BluetoothDevice device;
    private BluetoothSocket socket;
    private DataHandler dataHandler;

    public BluetoothManager(final BluetoothAdapter bluetoothAdapter, final UUID serviceUUID, final EventBus eventBus, final RedisManager redisManager) {
        this.bluetoothAdapter = bluetoothAdapter;
        this.serviceUUID = serviceUUID;
        this.eventBus = eventBus;
        this.redisManager = redisManager;
        this.connector = null;
        this.device = null;
        this.socket = null;
        this.dataHandler = null;
    }

    public boolean isBluetoohEnabled() {
        return bluetoothAdapter.isEnabled();
    }

    public boolean isConnected() {
        return (socket != null && socket.isConnected());
    }

    public void connect() throws IOException {
        discoverDevices();

        if(socket != null && socket.isConnected()) {
            throw new IOException("No socket and connection");
        }

        device = bluetoothAdapter.getRemoteDevice(device.getAddress());
        socket = device.createRfcommSocketToServiceRecord(serviceUUID);

        eventBus.post(new ConnectEvent(ConnectEventType.CONNECTING));

        try {
            socket.connect();
        } catch (IOException e) {
            eventBus.post(new ConnectEvent(ConnectEventType.CONNECTION_ERROR));
            throw e;
        }

        /**
          connecting the socket in another thread causes connection trouble??
          i was unable to track this down so I decieded to block the main thread shortly while connecting
          connector = new Connector(serviceUUID, device.getAddress());
          new Thread(connector).start();
        */

        dataHandler = new DataHandler(socket.getInputStream(), socket.getOutputStream());
        new Thread(dataHandler).start();

        eventBus.post(new ConnectEvent(ConnectEventType.CONNECTED));
    }

    public void disconnect() {
        eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTING));
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTED));
        }
    }

    public void sendMessage(final char[] message) throws IOException {
        if(dataHandler == null) {
            eventBus.post(new ConnectEvent(ConnectEventType.CONNECTION_ERROR));
            throw new IOException("Handler has not been initialized");
        }

        if(!socket.isConnected()) {
            eventBus.post(new ConnectEvent(ConnectEventType.CONNECTION_ERROR));
            throw new IOException("Socket is not connected");
        }
        dataHandler.enqueueForWrite(message);
    }

    private void discoverDevices() {
        for (BluetoothDevice device : bluetoothAdapter.getBondedDevices()) {
            this.device = device;
        }
    }

    private class Connector implements Runnable {

        private final UUID uuid;
        private final String address;
        private DataHandler dataHandler;
        private BluetoothSocket socket;

        private Connector(final UUID uuid, final String address) throws IOException {
            this.uuid = uuid;
            this.address = address;
            //this.socket = this.device.createRfcommSocketToServiceRecord(uuid);
            this.socket = bluetoothAdapter.getRemoteDevice(address).createRfcommSocketToServiceRecord(serviceUUID);
            System.out.println("Address: " + device.getAddress());
            System.out.println("UUID:" + device.getUuids()[0].getUuid());
            System.out.println("DeviceName: " + device.getName());

            this.dataHandler = null;
        }

        @Override
        public void run() {
            bluetoothAdapter.cancelDiscovery();

            try {
                if(socket.isConnected()) {
                    System.out.println("Already connected");
                } else {
                    System.out.println("Connecting");
                    socket.connect();
                    dataHandler = new DataHandler(socket.getInputStream(), socket.getOutputStream());
                    new Thread(dataHandler).start();
                }
            } catch (IOException e) {
                eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTED));
                e.printStackTrace();
            } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        public void disconnect() {
            eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTING));
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTED));
            }
        }

        public void sendMessage(final char[] message) throws IOException {
            if(dataHandler == null) {
                throw new IOException("Please connect before sending a message");
            }
            dataHandler.enqueueForWrite(message);
        }
    }

    private class DataHandler implements Runnable {

        private final InputStream inputStream;
        private final OutputStream outputStream;
        private final BufferedWriter bufferedWriter;
        private final Queue<char[]> queue;

        public DataHandler(final InputStream inputStream, final OutputStream outputStream) throws IOException {
            this.inputStream = inputStream;
            this.outputStream = outputStream;
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            this.queue = new ConcurrentLinkedQueue<>();
        }

        public void run() {
            while (true) {
                try {
                    char [] msg = queue.poll();
                    if(msg == null) {
                        continue;
                    }
                    Thread.sleep(10);
                    bufferedWriter.write(msg);
                    bufferedWriter.flush();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void enqueueForWrite(char[] msg) throws IOException {
            queue.add(msg);
        }

        public void cancel() throws IOException {
            eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTING));
            socket.close();
            eventBus.post(new ConnectEvent(ConnectEventType.DISCONNECTED));
        }
    }
}
