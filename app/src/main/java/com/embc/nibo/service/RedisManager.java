package com.embc.nibo.service;

import redis.clients.jedis.Jedis;

/**
 * @author Stefan Wirth
 * @date 13.12.2015
 * @note Unused due to lack of time
 */
public class RedisManager {
    private final Jedis jedis;

    public RedisManager(final Jedis jedis) {
        this.jedis = jedis;
    }

    public void publish(final String channelName, final String msg) {
        jedis.publish(channelName, msg);
    }
}
