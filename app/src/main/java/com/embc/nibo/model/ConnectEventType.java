package com.embc.nibo.model;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Connect event types
 */
public enum ConnectEventType {
    CONNECTING("Connecting"),
    CONNECTED("Connected"),
    DISCONNECTING("Disconnecting"),
    DISCONNECTED("Disconnected"),
    CONNECTION_ERROR("Connection error"),
    UNKNOWN("Unkown connection state");

    private final String description;

    private ConnectEventType(final String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "ConnectEventType{" +
                "description='" + description + '\'' +
                '}';
    }
}
