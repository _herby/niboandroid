package com.embc.nibo.model;

/**
 * @author Stefan Wirth
 * @date 14.12.2015
 * @brief Types fo acccelerometer events
 */
public enum AcceleratorEventType {
    LEFT('0'),
    LEFT_FORWARDS('1'),
    LEFT_BACKWARDS('2'),
    RIGHT('3'),
    RIGHT_FORWARDS('4'),
    RIGHT_BACKWARDS('5'),
    FORWARDS('6'),
    BACKWARDS('7'),
    STANDING('8');

    private final char charValue;

    private AcceleratorEventType(final char charValue) {
        this.charValue = charValue;
    }

    public char getCharValue() {
        return charValue;
    }
}
