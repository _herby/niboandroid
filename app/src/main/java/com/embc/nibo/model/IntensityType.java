package com.embc.nibo.model;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Different intensity types
 */
public enum IntensityType {
    NONE('0'),
    LOW('1'),
    MEDIUM('2'),
    HIGH('3');

    private final char charValue;

    private IntensityType(char charValue) {
        this.charValue = charValue;
    }

    public static IntensityType typeForTilt(final float tilt) {
        final int value = Math.round(Math.abs(tilt) / 3);
        assert(value < IntensityType.values().length);

        return IntensityType.values()[value];
    }

    public char getCharValue() {
        return charValue;
    }
}
