package com.embc.nibo.model;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Model for different connect events
 */

public class ConnectEvent {
    private final ConnectEventType connectEventType;

    public ConnectEvent(final ConnectEventType connectEventType) {
        this.connectEventType = connectEventType;
    }

    public ConnectEventType getConnectEventType() {
        return connectEventType;
    }

    @Override
    public String toString() {
        return "ConnectEvent{" +
                "connectEventType=" + connectEventType +
                '}';
    }
}
