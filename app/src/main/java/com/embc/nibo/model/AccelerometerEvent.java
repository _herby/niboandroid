package com.embc.nibo.model;

/**
 * @author Stefan Wirth
 * @date 21.11.2015
 * @brief Model for accelerometerevents
 */
public class AccelerometerEvent {

    private final IntensityType intensityType;
    private final AcceleratorEventType acceleratorEventType;

    public AccelerometerEvent(final IntensityType intensityType, final AcceleratorEventType acceleratorEventType) {
        this.intensityType = intensityType;
        this.acceleratorEventType = acceleratorEventType;
    }

    public IntensityType getIntensityType() {
        return intensityType;
    }

    public AcceleratorEventType getAcceleratorEventType() {
        return acceleratorEventType;
    }

    @Override
    public String toString() {
        return "AccelerometerEvent{" +
                "intensityType=" + intensityType +
                ", acceleratorEventType=" + acceleratorEventType +
                '}';
    }

    public char[] toMessage() {
        return new char[] {
            '!',
            intensityType.getCharValue(),
            acceleratorEventType.getCharValue(),
            '?'
        };
    }
}
