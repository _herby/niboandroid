package com.embc.nibo;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.embc.nibo.model.AccelerometerEvent;
import com.embc.nibo.model.ConnectEvent;
import com.embc.nibo.service.AccelerometerManager;
import com.embc.nibo.service.BluetoothManager;
import com.embc.nibo.service.RedisManager;

import java.io.IOException;
import java.util.UUID;

import de.greenrobot.event.EventBus;
import redis.clients.jedis.Jedis;


public class MainActivity extends ActionBarActivity {

    private AccelerometerManager accelerometerManager;
    private BluetoothManager bluetoothManager;
    private EventBus eventBus = EventBus.getDefault();
    private TextView connectionStateLabel;
    private TextView drivingDirectionLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        connectionStateLabel = (TextView) findViewById(R.id.connectionStateLabel);
        drivingDirectionLabel = (TextView) findViewById(R.id.drivingDirectionLabel);

        //those would probly go inside a controller not the view
        final SensorManager manager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);

        accelerometerManager = new AccelerometerManager(manager, eventBus);
        bluetoothManager = new BluetoothManager(
            BluetoothAdapter.getDefaultAdapter(),
            UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"),
            eventBus,
            new RedisManager(new Jedis("192.168.178.47"))
        );

        eventBus.register(this);
    }

    //this should be inside the button action
    private void connect() {
        try {
            bluetoothManager.connect();
        } catch (IOException e) {
            //show user that there was an error
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        accelerometerManager.registerListerner();
    }

    @Override
    protected void onPause() {
        super.onPause();
        accelerometerManager.unRegisterListerner();
    }

    public void connectButtonPressed(final View view) {
        connect();
    }

    public void disconnectButtonPressed(final View view) {
        bluetoothManager.disconnect();
    }

    public void onEvent(final ConnectEvent connectEvent) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectionStateLabel.setText(connectEvent.getConnectEventType().getDescription());
            }
        });
    }

    //called for eventbus via reflection API
    public void onEvent(final AccelerometerEvent event) {
        //System.out.println(event);
        if(!bluetoothManager.isConnected()) {
            return;
        }

        drivingDirectionLabel.setText(event.toString());

        try {
            bluetoothManager.sendMessage(event.toMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
